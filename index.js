addListeners();

function animaster () {
  function getTransform (translation, ratio, rotate, transform) {
    const result = [];
    if (translation) {
      transform += `translate(${translation.x}px,${translation.y}px)`;
    }
    if (ratio) {
      transform += `scale(${ratio})`;
    }
    if (rotate) {
      transform += `rotate(${rotate}deg)`;
    }
    return transform;
  }

  function resetFadeIn (element) {
    element.style.transitionDuration = null;
    element.classList.remove("show");
    element.classList.add("hide");
  }

  function resetFadeOut (element) {
    element.style.transitionDuration = null;
    element.classList.remove("hide");
    element.classList.add("show");
  }

  function resetMoveAndScale (element) {
    element.style.transitionDuration = null;
    element.style.transform = null;
  }

  function resetMoveAndHide (element) {
    resetFadeOut(element);
    resetMoveAndScale(element);
  }

  return {

    _steps: [],

    buildHandler: function () {
      let that = this;
      return function () {
          let element = this;
          that.play(element);
        };
    },

    addRotate: function (duration, params) {
      this._steps.push({name: "rotate", duration: duration, params: params});
      return this;
    },

    addMove: function (duration, params){
      this._steps.push({name: "move", duration: duration, params: params});
      return this;
    },

    addScale: function (duration, params){
      this._steps.push({name: "scale", duration: duration, params: params});
      return this;
    },

    addFadeIn: function (duration, params){
      this._steps.push({name: "fadeIn", duration: duration, params: params});
      return this;
    },

    addFadeOut: function (duration, params) {
      this._steps.push({name: "fadeOut", duration: duration, params: params});
      return this;
    },

    addDelay: function (delay) {
      setTimeout(()=>{}, delay)
    },

    anims: [],

    transform: "",

    trDuration: "",

    play: function (elem, cycled) {



        if(this._steps.length === 0) return;
        let step = this._steps.shift();
        if(cycled) {
            this._steps.push(step);
            elem.style.transform = "";
        }

        switch(step.name) {
          case "move":
            elem.style.transitionDuration = `${step.duration}ms`;
            elem.style.transform = getTransform(step.params, null, null, elem.style.transform);
            this.anims.push(resetMoveAndScale);
            break;
          case "fadeIn":
            elem.style.transitionDuration = `${step.duration}ms`;
            elem.classList.remove("hide");
            elem.classList.add("show");
            this.anims.push(resetFadeIn)
            break;
          case "fadeOut":
            elem.style.transitionDuration = `${step.duration}ms`;
            elem.classList.remove("show");
            elem.classList.add("hide");
            this.anims.push(resetFadeOut);
            break;
          case "scale":
            elem.style.transitionDuration = `${step.duration}ms`;
            elem.style.transform = getTransform(null, step.params, null, elem.style.transform);
            this.anims.push(resetMoveAndScale);
            break;
          case "rotate":
            elem.style.transitionDuration = `${step.duration}ms`;
            elem.style.transform = getTransform(null, null, step.params, elem.style.transform);
            break;
        }
        setTimeout(() => this.play(elem, cycled), step.duration);

        return {
          reset: () => {
            for(let i = 0; i < this.anims.length; i++){
              this.anims[i](elem);
            }
          },
          stop: () => {
            cycled = false;
            this._steps = [];
          }

        };
    },


    fadeIn: function (element, duration) {
      this.addFadeIn(duration).play(element);
    },

    fadeOut: function (element, duration) {
      this.addFadeOut(duration).play(element);
    },
    move: function (elem) {
      this.addMove(duration, params).play(elem);
    },
    scale: function (element, duration, params) {
      this.addScale(duration, params).play(element);
    },
    moveAndHide: function (element, duration) {

      this.addMove(duration * 0.4, { x: 100, y: 20 });
      this.addFadeOut(duration * 0.6);

      return this.play(element);

    },
    showAndHide: function (element, duration) {
      duration = duration / 3;
      this.addFadeIn(duration);
      this.addDelay(duration);
      this.addFadeOut(duration);
      this.play(element);
    },
    heartBeating: function (element) {
      this.addScale(500, 1.4).addScale(500, 1);
      return this.play(element, true);
  }
}
}
  const worryAnimationHandler = animaster()
    .addMove(200, {x: 80, y: 0})
    .addMove(500, {x: 0, y: 0})
    .addMove(200, {x: 80, y: 0})
    .addMove(5000, {x: 0, y: 0})
    .buildHandler();

  const myAnim = animaster()
    .addRotate(1000, 90)
    .addRotate(750, 180)
    .addRotate(500, 270)
    .addRotate(1000, 360)
    .buildHandler();

    const customAnimation = animaster()
    .addMove(200, {x: 40, y: 40})
    .addScale(800, 1.3)
    .addMove(200, {x: 0, y: -80})
    .addScale(800, 1)
    .addMove(200, {x: -80, y: 0})
    .addScale(800, 0.8)
    .addMove(200, {x: 40, y: 40})
    .buildHandler();

function addListeners() {
  document.getElementById("fadeInPlay").addEventListener("click", function() {
    const block = document.getElementById("fadeInBlock");
    const reset = animaster().addFadeIn(5000).play(block);
    function resetEvent() {
      reset.reset();
      document
        .getElementById("fadeInReset")
        .removeEventListener("click", resetEvent);
    }
    document
      .getElementById("fadeInReset")
      .addEventListener("click", resetEvent);
  });

  document.getElementById("movePlay").addEventListener("click", function() {
    const block = document.getElementById("moveBlock");
    const reset = animaster().addMove(1000, { x: 100, y: 10 }).play(block);
    function resetEvent() {
      reset.reset();
      document
        .getElementById("moveReset")
        .removeEventListener("click", resetEvent);
    }

    document.getElementById("moveReset").addEventListener("click", resetEvent);
  });

  document.getElementById("scalePlay").addEventListener("click", function() {
    const block = document.getElementById("scaleBlock");
    const reset = animaster().addScale(1000, 1.25).play(block);
    function resetEvent() {
      reset.reset();
      document
        .getElementById("scaleReset")
        .removeEventListener("click", resetEvent);
    }

    document.getElementById("scaleReset").addEventListener("click", resetEvent);
  });
  document
    .getElementById("moveAndHidePlay")
    .addEventListener("click", function() {
      const block = document.getElementById("moveAndHideBlock");
      const stop = animaster().moveAndHide(block, 5000);
      function resetEvent() {
        stop.reset();
        document
          .getElementById("moveAndHideReset")
          .removeEventListener("click", resetEvent);
      }
      document
        .getElementById("moveAndHideReset")
        .addEventListener("click", resetEvent);
    });
  document
    .getElementById("showAndHidePlay")
    .addEventListener("click", function() {
      const block = document.getElementById("showAndHideBlock");
      animaster().showAndHide(block, 3000);
    });
  document
    .getElementById("heartBeatingPlay")
    .addEventListener("click", function() {
      const block = document.getElementById("heartBeatingBlock");

      const stop = animaster().heartBeating(block);
      function resetEvent() {
        stop.stop();
        document
          .getElementById("heartBeatingStop")
          .removeEventListener("click", resetEvent);
      }

      document
        .getElementById("heartBeatingStop")
        .addEventListener("click", resetEvent);
    });
}
  document
    .getElementById("worryAnimationBlock")
    .addEventListener("click", worryAnimationHandler);

  document
      .getElementById("myBlock")
      .addEventListener("click", customAnimation);
